function downloadFile() {

    var title = document.getElementById('title').value;
    var content = document.getElementById('content').value;
    var filename = title+".txt";
    var text = title + '\n\n---\n\n' + content;

    if(title == '') {
        download('ritr.txt',text);
    }
    else {
        download(filename,text);
    }
}

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function clearFields() {
    document.getElementById('content').value = '';
    document.getElementById('display-count').textContent = 0;
}

function wordCount() {

    var textArea = document.getElementById('content');
    var words = textArea.value.match(/\S+/g).length;

    var wordCount = document.getElementById('display-count');

    if(textArea.value == '') {
        wordCount.textContent = 0;
    }
    else {
        wordCount.textContent = words;
    }
}